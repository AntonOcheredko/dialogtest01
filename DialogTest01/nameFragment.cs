﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DialogTest01
{
    class nameFragment : DialogFragment
    {
        private ListView lv;
        private string[] nameArray = { "Alexey allmighty", "Alexander", "Andrew boss", "Anton newbie", "Hripa" };

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View v = inflater.Inflate(Resource.Layout.fragmentLayout, container, false);

            this.Dialog.SetTitle("Name List");

            lv = v.FindViewById<ListView>(Resource.Id.lv);


            ArrayAdapter adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, nameArray);
            lv.Adapter = adapter;
            lv.ItemClick += Lv_ItemClick;
            return v;
        }

        private void Lv_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            Toast.MakeText(this.Activity, nameArray[e.Position], ToastLength.Short).Show();
        }
    }


}